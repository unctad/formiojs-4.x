"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ComponentEdit = _interopRequireDefault(require("../../_classes/component/editForm/Component.edit.conditional"));

var _ComponentEdit2 = _interopRequireDefault(require("../../_classes/component/editForm/Component.edit.logic"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = [{
  key: 'labelPosition',
  ignore: true
}, {
  key: 'placeholder',
  ignore: true
}, {
  key: 'description',
  ignore: true
}, {
  key: 'autofocus',
  ignore: true
}, {
  key: 'tooltip',
  ignore: true
}, {
  key: 'tabindex',
  ignore: true
}, {
  key: 'disabled',
  ignore: true
}, {
  key: 'tableView',
  ignore: true
}, {
  key: 'components',
  type: 'datagrid',
  input: true,
  label: 'Tabs',
  weight: 50,
  reorder: true,
  components: [{
    type: 'textfield',
    input: true,
    key: 'label',
    label: 'Label'
  }, {
    type: 'textfield',
    input: true,
    key: 'key',
    label: 'Key',
    allowCalculateOverride: true,
    calculateValue: {
      _camelCase: [{
        var: 'row.label'
      }]
    }
  }, {
    'collapsible': false,
    'dataGridLabel': true,
    'modalEdit': true,
    'key': 'tab-conditional',
    'type': 'panel',
    'label': 'Conditional',
    'input': false,
    'tableView': false,
    'components': _ComponentEdit.default
  }, {
    'collapsible': false,
    'dataGridLabel': true,
    'modalEdit': true,
    'key': 'tab-field-logic',
    'type': 'panel',
    'label': 'Logic',
    'input': false,
    'tableView': false,
    'components': _ComponentEdit2.default
  }]
}, {
  weight: 1100,
  type: 'checkbox',
  label: 'Vertical Layout',
  tooltip: 'Make this field display in vertical orientation.',
  key: 'verticalLayout',
  input: true
}];
exports.default = _default;