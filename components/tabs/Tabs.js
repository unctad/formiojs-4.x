"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

require("core-js/modules/es.reflect.get.js");

require("core-js/modules/es.object.get-own-property-descriptor.js");

require("core-js/modules/es.reflect.construct.js");

require("core-js/modules/es.array.slice.js");

require("core-js/modules/es.function.name.js");

require("core-js/modules/es.array.from.js");

require("core-js/modules/es.string.iterator.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.symbol.js");

require("core-js/modules/es.symbol.description.js");

require("core-js/modules/es.symbol.iterator.js");

require("core-js/modules/es.array.iterator.js");

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.weak-map.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.array.concat.js");

require("core-js/modules/es.array.map.js");

require("core-js/modules/es.array.splice.js");

require("core-js/modules/es.object.to-string.js");

require("core-js/modules/web.dom-collections.for-each.js");

require("core-js/modules/es.array.find-index.js");

require("core-js/modules/es.array.filter.js");

require("core-js/modules/es.array.includes.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.object.get-prototype-of.js");

var _lodash = _interopRequireDefault(require("lodash"));

var _NestedComponent2 = _interopRequireDefault(require("../_classes/nested/NestedComponent"));

var FormioUtils = _interopRequireWildcard(require("../../utils/utils"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var TabsComponent = /*#__PURE__*/function (_NestedComponent) {
  _inherits(TabsComponent, _NestedComponent);

  var _super = _createSuper(TabsComponent);

  function TabsComponent() {
    var _this;

    _classCallCheck(this, TabsComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.currentTab = 0;
    _this.noField = true;
    return _this;
  }

  _createClass(TabsComponent, [{
    key: "defaultSchema",
    get: function get() {
      return TabsComponent.schema();
    }
  }, {
    key: "schema",
    get: function get() {
      var _this2 = this;

      var schema = _get(_getPrototypeOf(TabsComponent.prototype), "schema", this); // We need to clone this because the builder uses the "components" reference and this would reset that reference.


      var components = _lodash.default.cloneDeep(this.component.components);

      schema.components = components.map(function (tab, index) {
        tab.components = _this2.tabs[index].map(function (component) {
          return component.schema;
        });
        return tab;
      });
      return schema;
    }
  }, {
    key: "tabKey",
    get: function get() {
      return "tab-".concat(this.key);
    }
  }, {
    key: "tabLikey",
    get: function get() {
      return "tabLi-".concat(this.key);
    }
  }, {
    key: "tabLinkKey",
    get: function get() {
      return "tabLink-".concat(this.key);
    }
  }, {
    key: "checkComponentConditions",
    value: function checkComponentConditions(data, flags, row) {
      if (!_get(_getPrototypeOf(TabsComponent.prototype), "checkComponentConditions", this).call(this, data, flags, row)) {
        return false;
      }

      var oldTabItemComponents = _lodash.default.cloneDeep(this.tabItemComponents);

      this.createTabs(false);

      if (!_lodash.default.isEqual(oldTabItemComponents, this.tabItemComponents)) {
        this.redraw();
      }

      return true;
    }
  }, {
    key: "init",
    value: function init() {
      this.components = [];
      this.createTabs();
    }
  }, {
    key: "createTabs",
    value: function createTabs() {
      var _this3 = this;

      var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.tabItemComponents = this.tabItemComponents || [];
      this.tabs = this.tabs || [];
      var currentTabItemComponent = this.tabItemComponents[this.currentTab];
      var index = 0;

      var _iterator = _createForOfIteratorHelper(this.component.components),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var tabItemComponent = _step.value;
          var hasCondition = FormioUtils.hasCondition(tabItemComponent);

          if (this.builderMode || this.previewMode || hasCondition && FormioUtils.checkCondition(tabItemComponent, this.data, this.rootValue, this.root ? this.root._form : {}, this) || !hasCondition && !tabItemComponent.hidden) {
            if (init || this.tabItemComponents[index] !== tabItemComponent) {
              this.tabItemComponents[index] = tabItemComponent;

              if (!tabItemComponent.components) {
                tabItemComponent.components = [];
              }

              this.tabs[index] = tabItemComponent.components.map(function (comp) {
                var component = _this3.createComponent(comp);

                component.tab = index;
                return component;
              });
            }

            index++;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      if (this.tabItemComponents.length > index) {
        this.tabItemComponents.splice(index);
        this.tabs.splice(index);
      }

      if (currentTabItemComponent) {
        this.currentTab = this.tabItemComponents.indexOf(currentTabItemComponent);

        if (this.currentTab === -1) {
          this.currentTab = 0;
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return _get(_getPrototypeOf(TabsComponent.prototype), "render", this).call(this, this.renderTemplate('tab', {
        tabKey: this.tabKey,
        tabLikey: this.tabLikey,
        tabLinkKey: this.tabLinkKey,
        currentTab: this.currentTab,
        tabItemComponents: this.tabItemComponents,
        tabComponents: this.tabs.map(function (tab) {
          return _this4.renderComponents(tab);
        })
      }, this.options.flatten || this.options.pdf ? 'flat' : null));
    }
  }, {
    key: "attach",
    value: function attach(element) {
      var _this$loadRefs,
          _this5 = this;

      this.loadRefs(element, (_this$loadRefs = {}, _defineProperty(_this$loadRefs, this.tabLinkKey, 'multiple'), _defineProperty(_this$loadRefs, this.tabKey, 'multiple'), _defineProperty(_this$loadRefs, this.tabLikey, 'multiple'), _this$loadRefs));
      ['change', 'error'].forEach(function (event) {
        return _this5.on(event, _this5.handleTabsValidation.bind(_this5));
      });

      var superAttach = _get(_getPrototypeOf(TabsComponent.prototype), "attach", this).call(this, element);

      this.refs[this.tabLinkKey].forEach(function (tabLink, index) {
        _this5.addEventListener(tabLink, 'click', function (event) {
          event.preventDefault();

          _this5.setTab(index);
        });
      });
      this.refs[this.tabKey].forEach(function (tab, index) {
        _this5.attachComponents(tab, _this5.tabs[index], _this5.component.components[index].components);
      });
      return superAttach;
    }
  }, {
    key: "detachLogic",
    value: function detachLogic() {
      _get(_getPrototypeOf(TabsComponent.prototype), "detachLogic", this).call(this);

      var _iterator2 = _createForOfIteratorHelper(this.component.components),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var tabItemComponent = _step2.value;

          if (!tabItemComponent.logic) {
            continue;
          }

          var _iterator3 = _createForOfIteratorHelper(tabItemComponent.logic),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var logic = _step3.value;

              if (logic.trigger.type !== 'event') {
                continue;
              }

              var _event = this.interpolate(logic.trigger.event);

              this.off(_event);
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
  }, {
    key: "fieldLogic",
    value: function fieldLogic(data, row) {
      var _this6 = this;

      data = data || this.rootValue;
      row = row || this.data;
      var logics = this.logic; // If there aren't logic, don't go further.

      if (logics.length === 0 && (!this.originalComponent.components || !this.originalComponent.components.some(function (c) {
        return c.logic && c.logic.length > 0;
      }))) {
        return;
      }

      var newComponent = (0, FormioUtils.fastCloneDeep)(this.originalComponent);
      var changed = logics.reduce(function (changed, logic) {
        var result = FormioUtils.checkTrigger(newComponent, logic.trigger, row, data, _this6.root ? _this6.root._form : {}, _this6);
        return (result ? _this6.applyActions(newComponent, logic.actions, result, row, data) : false) || changed;
      }, false);

      var _iterator4 = _createForOfIteratorHelper(newComponent.components),
          _step4;

      try {
        var _loop = function _loop() {
          var tabItemComponent = _step4.value;

          if (!tabItemComponent.logic) {
            return "continue";
          }

          tabItemComponent.logic.forEach(function (logic) {
            var result = FormioUtils.checkTrigger(tabItemComponent, logic.trigger, row, data, _this6.root ? _this6.root._form : {}, _this6);
            changed = (result ? _this6.applyActions(tabItemComponent, logic.actions, result, row, data) : false) || changed;
          });
        };

        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var _ret = _loop();

          if (_ret === "continue") continue;
        } // If component definition changed, replace and mark as changed.

      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }

      if (!_lodash.default.isEqual(this.component, newComponent)) {
        this.component = newComponent;
        changed = true;
        var disabled = this.shouldDisabled; // Change disabled state if it has changed

        if (this.disabled !== disabled) {
          this.disabled = disabled;
        }
      }

      return changed;
    }
  }, {
    key: "attachLogic",
    value: function attachLogic() {
      var _this7 = this;

      // Do not attach logic during builder mode.
      if (this.builderMode) {
        return;
      }

      _get(_getPrototypeOf(TabsComponent.prototype), "attachLogic", this).call(this);

      var _loop2 = function _loop2(i) {
        var tabItemComponent = _this7.component.components[i];

        if (!tabItemComponent.logic) {
          return "continue";
        }

        var _iterator5 = _createForOfIteratorHelper(tabItemComponent.logic),
            _step5;

        try {
          var _loop3 = function _loop3() {
            var logic = _step5.value;

            if (logic.trigger.type !== 'event') {
              return "continue";
            }

            _this7.on(event, function () {
              var newTabItemComponent = (0, FormioUtils.fastCloneDeep)(tabItemComponent);

              for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                args[_key2] = arguments[_key2];
              }

              if (_this7.applyActions(newTabItemComponent, logic.actions, args)) {
                // If component definition changed, replace it.
                tabItemComponent = _this7.component.components[i];

                if (!_lodash.default.isEqual(tabItemComponent, newTabItemComponent)) {
                  _this7.component.components[_this7.component.components.indexOf(tabItemComponent)] = newTabItemComponent;

                  _this7.redraw();
                }
              }
            }, true);
          };

          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var _ret3 = _loop3();

            if (_ret3 === "continue") continue;
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      };

      for (var i = 0; i < this.component.components.length; i++) {
        var _ret2 = _loop2(i);

        if (_ret2 === "continue") continue;
      }
    }
  }, {
    key: "detach",
    value: function detach(all) {
      _get(_getPrototypeOf(TabsComponent.prototype), "detach", this).call(this, all);
    }
    /**
     * Set the current tab.
     *
     * @param index
     */

  }, {
    key: "setTab",
    value: function setTab(index) {
      var _this8 = this;

      if (!this.tabs || !this.tabs[index] || !this.refs[this.tabKey] || !this.refs[this.tabKey][index] || this.tabItemComponents[index].disabled) {
        return;
      }

      this.currentTab = index;

      _lodash.default.each(this.refs[this.tabKey], function (tab) {
        _this8.removeClass(tab, 'formio-tab-panel-active');

        tab.style.display = 'none';
      });

      this.addClass(this.refs[this.tabKey][index], 'formio-tab-panel-active');
      this.refs[this.tabKey][index].style.display = 'block';

      _lodash.default.each(this.refs[this.tabLinkKey], function (tabLink, tabIndex) {
        if (_this8.refs[_this8.tabLinkKey][tabIndex]) {
          _this8.removeClass(tabLink, 'formio-tab-link-active');
        }

        if (_this8.refs[_this8.tabLikey][tabIndex]) {
          _this8.removeClass(_this8.refs[_this8.tabLikey][tabIndex], 'formio-tab-link-container-active');
        }
      });

      if (this.refs[this.tabLikey][index]) {
        this.addClass(this.refs[this.tabLikey][index], 'formio-tab-link-container-active');
      }

      if (this.refs[this.tabLinkKey][index]) {
        this.addClass(this.refs[this.tabLinkKey][index], 'formio-tab-link-active');
      }

      this.triggerChange();
    }
  }, {
    key: "beforeFocus",
    value: function beforeFocus(component) {
      if ('beforeFocus' in this.parent) {
        this.parent.beforeFocus(this);
      }

      var tabIndex = this.tabs.findIndex(function (tab) {
        return tab.some(function (comp) {
          return comp === component;
        });
      });

      if (tabIndex !== -1 && this.currentTab !== tabIndex) {
        this.setTab(tabIndex);
      }
    }
  }, {
    key: "setErrorClasses",
    value: function setErrorClasses(elements, dirty, hasErrors, hasMessages) {
      var _this9 = this;

      var element = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : this.element;

      if (this.component.modalEdit) {
        _get(_getPrototypeOf(TabsComponent.prototype), "setErrorClasses", this).call(this, elements, dirty, hasErrors, hasMessages, element);
      }

      elements.forEach(function (element) {
        _this9.addClass(element, 'is-invalid');

        if (element.getAttribute('ref') !== 'openModal') {
          if (_this9.options.highlightErrors) {
            _this9.addClass(element, 'tab-error');
          } else {
            _this9.addClass(element, 'has-error');
          }
        }
      });
    }
  }, {
    key: "clearErrorClasses",
    value: function clearErrorClasses(elements) {
      var _this10 = this;

      if (this.component.modalEdit) {
        var element = Array.isArray(elements) || elements instanceof NodeList ? this.element : elements;

        _get(_getPrototypeOf(TabsComponent.prototype), "clearErrorClasses", this).call(this, element);
      }

      elements = Array.isArray(elements) || elements instanceof NodeList ? elements : [elements];
      elements.forEach(function (element) {
        _this10.removeClass(element, 'is-invalid');

        _this10.removeClass(element, 'tab-error');

        _this10.removeClass(element, 'has-error');
      });
    }
  }, {
    key: "handleTabsValidation",
    value: function handleTabsValidation() {
      if (!this.refs[this.tabLinkKey] || !this.refs[this.tabLinkKey].length || !this.tabs.length) {
        return;
      }

      this.clearErrorClasses(this.refs[this.tabLinkKey]);
      var invalidTabsIndexes = this.tabs.reduce(function (invalidTabs, tab, tabIndex) {
        var hasComponentWithError = tab.some(function (comp) {
          return !!comp.error;
        });
        return hasComponentWithError ? [].concat(_toConsumableArray(invalidTabs), [tabIndex]) : invalidTabs;
      }, []);

      if (!invalidTabsIndexes.length) {
        return;
      }

      var invalidTabs = _toConsumableArray(this.refs[this.tabLinkKey]).filter(function (_, tabIndex) {
        return invalidTabsIndexes.includes(tabIndex);
      });

      this.setErrorClasses(invalidTabs);
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len3 = arguments.length, extend = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        extend[_key3] = arguments[_key3];
      }

      return _NestedComponent2.default.schema.apply(_NestedComponent2.default, [{
        label: 'Tabs',
        type: 'tabs',
        input: false,
        key: 'tabs',
        persistent: false,
        tableView: false,
        components: [{
          label: 'Tab 1',
          key: 'tab1',
          components: []
        }],
        verticalLayout: false
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Tabs',
        group: 'layout',
        icon: 'folder-o',
        weight: 50,
        documentation: '/userguide/#tabs',
        schema: TabsComponent.schema()
      };
    }
  }]);

  return TabsComponent;
}(_NestedComponent2.default);

exports.default = TabsComponent;